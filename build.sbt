name := """play-chatroom-java"""

version := "1.0-SNAPSHOT"

scalaVersion := "2.11.8"

lazy val root = (project in file(".")).enablePlugins(PlayJava)

libraryDependencies += "com.typesafe.play" %% "play" % "2.5.10"

