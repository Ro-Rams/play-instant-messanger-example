# play-instant-messanger-example

## Instructions

- Install scala.
- Open the project in your favourite IDE.
- Run the SBT file to get the dependencies.
- Run Application.java.

Your app should now be running on [localhost:9000](http://localhost:9000/).