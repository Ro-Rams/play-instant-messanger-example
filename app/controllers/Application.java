package controllers;

import models.Message;
import models.Messages;
import play.*;
import play.mvc.*;

import views.html.*;

import static play.data.Form.form;

public class Application extends Controller {

    public Result getMessages() {
        return ok(Messages.getMessages());
    }

    public Result sendMessage(final String user, final String message) {
        Messages.addMessage(new Message(user, message));
        return ok();
    }

}